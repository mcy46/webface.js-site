---
title: Component initialization - Webface.js
---

- @page_id = "component-initialization"
.title
  %h1 Component initialization

.content

  %p
    As was mentioned earlier, Webface.js scans the DOM and creates components on the fly.
    It works recursively: an already initialized component runs an
    <code class="inline language-javascript">initChildComponent()</code> method and scans its
    <code class="inline language-html">.innerHtml</code> for elements with
    <code class="inline language-html">data-component-class</code> attributes.
    When it finds such dom element, it creates an instance of a class that's specified as a value for this attribute
    and then calls <code class="inline language-javascript">initChildComponents()</code>
    on it too and so on. And it all starts with
    <code class="inline language-javascript">RootComponent</code> which is initialized by Webface.js app.

  %h2 Initialization tasks
  %p
    Let us now take a closer look at what happens during initialization in the Javascript code.
    Here are the things that happen under the hood when a new Component is created:

  %ul
    %li Validations are initialized
    %li
      The component is assigned a template DOM element if one is present in the DOM:
      the one with <code class="inline language-html">data-component-template</code>
      attribute with the value matching component class name.
    %li
      The associated <code class="inline language-javascript">HtmlElement</code>
      is assigned to the <code class="inline language-javascript">Component.dom_element</code> property
    %li
      The element is added into the children list of its parent and the parent
      component is accessible through the <code class="inline language-javascript">.parent</code> property.
    %li
      <code class="inline language-javascript">afterInitialize()</code> hook is run.
    %li
      The initialized component then scans for elements with <code class="inline language-html">data-component-class</code>
      attributes, creates components out of them and repeats this list for each child component.

  %p
    For you to
    be able to control the initialization process, you need to be aware of a few places in the component class
    that are involved in the initialization.

  %h2 The Constructor
  %p Strictly speaking, you only need an empty constructor for everything to work

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class MyNotificationComponent extends extend_as("MyNotificationcomponent").mix(Component).with() {
          constructor() {
            super();
          }
        }

  %p
    This will invoke <code class="inline language-javascript">Component</code>'s constructor which will
    will do all the underlying work of initializing the component. Just don't forget that
    the <code class="inline language-javascript">super()</code> call must always be there.

  %p.note
    <code class="inline language-javascript">Component.constructor()</code> takes an optional argument
    which is called <code class="inline language-javascript">options</code>. It is an object and the only option currently
    supported is <code class="inline language-javascript">_template_name</code>. Refer to the
    <a href="/templates.html">Templates</a> section for more information. In your own components, you may employ this argument
    for other things (like setting attribute values programmatically), while the
    <code class="inline language-javascript">_template_name</code> option will still be passed to the
    base <code class="inline language-javascript">Component.constructor()</code>.

  %p
    However, the constructor is where a lot of important declarations are put, specifically, you will find you want the
    following things declared in a constructor:
    %ul
      %li attribute names, default values and other settings for attributes
      %li handlers for events and other event settings
      %li
        other component-specific settings that are not necessarily present in the base
        <code class="inline language-javascript">Component</code> class

  %p
    Due to the current limitations of Javascript, these things cannot be declared at a class-level (like you would,
    do in Ruby, for example). So this is what these declarations may look like:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class MyNotificationComponent extends extend_as("MyNotificationcomponent").mix(Component).with() {
          constructor() {
            super();

            /********************** everything attribute-related ***************************************/
            this.attribute_names = ["message", "autohide_delay", "permanent", "message_id", "visible"];
            this.default_attribute_values = { "permanent": false, "autohide_delay": 5000 };
            this.attribute_callbacks = {
              this.attribute_callbacks["visible"] = (name, self) => {
                // do something when notification changes from visible to hidden;
              }
            };

            /********************** everything events related ******************************************/
            this.native_events   = ["close.click", "message.click"];
            this.event_handlers.add({
              event: this.click_event, role: "self.close", handler: (self, event) => self.hide()
            });

           }
         }

  %p
    You can learn more about those declarations and how they're used in the relevant sections on
    #{link_to "Attributes", "/attributes.html"} and #{link_to "Events", "/events.html"}. The point of this
    section is to just get you familiar with the components initialization process.


  %h2 afterInitialize()
  a(name="afterInitialize")
  %p
    Unfortunately, most of the useful code you might want to run on component
    initialization cannot be put into the constructor, because at this point
    the dom_element is not assigned, parent is not assigned and children are
    not initialized. Therefore, the <code class="inline language-javascript">afterInitialize()</code>
    method is introduced - just override it in your class and it will be run after a component is
    initialized.

  %p
    By default, the only things <code class="inline language-javascript">afterInitialize()</code>
    does are:

    %ul
      %li initialize #{link_to "Behaviors", "/behaviors.html"}
      %li sets default attributes
      %li initializes i18n validation error messages
    You can redefine this method in the descendant classes you create,
    but don't forget to call <code class="inline language-javascript">super()</code>!

  %p
    Here's a typical example where <code class="inline language-javascript">afterInitialize()</code>
    becomes useful - we create a component and then we automatically read its property values from the
    corresponding DOM elements and attributes:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class MyComponent extends extend_as("Mycomponent").mix(Component).with() {
          afterInitialize() {
            super();
            this.updatePropertiesFromNodes({ invoke_callbacks: true });
          }
        }

  %p
    For more info on <code class="inline language-javascript">updatePropertiesFromNodes()</code>
    method see the #{link_to "Properties: updating from DOM section", "/properties"}.



  = partial "shared/nav", locals: { prev: [nil, "Introduction to components", "introduction-to-components"], nxt: [nil, "RootComponent", "root-component"]}
