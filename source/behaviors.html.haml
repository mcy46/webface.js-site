---
title: Behaviors - Webface.js
---

- @page_id = "behaviors"
.title
  %h1 Behaviors

.content

  %p
    Behaviors are special methods that usually manipulate the associated dom elements
    of components. For example, we might want to hide or disable a button - those are behaviors.
    To invoke a behavior you call a <code class="inline language-javascript">behave()</code> method on a component
    and pass it the intended behavior name:

  %pre
    %code.language-javascript
      :erb
        button1.behave("disable"); 

  %p
    It is quite common to invoke behaviors from within components in response
    to a certain event that was triggered. For example we could disable a button if
    someone clicks it:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {

          this.native_events = ["click"];

          consrtuctor() {
            event_handlers.add({
              event: 'click',
              role:  #self,
              handler: (self,event) => self.behave('disable')
            });
          }
        }

  %p
    There are some behaviors which Webface.js defines for all components, others are only available
    for some (see <a href="https://gitlab.com/hodl/webface.js/tree/master/lib/behaviors">behavior classess in the repository</a>).
    All components have the following behaviors available:
    
    %ul
      %li <code class="inline language-javascript">hide()</code>
      %li <code class="inline language-javascript">show()</code>
      %li <code class="inline language-javascript">disable()</code>
      %li <code class="inline language-javascript">enable()</code>
      %li <code class="inline language-javascript">lock()</code>
      %li <code class="inline language-javascript">unlock()</code>


  %p.note
    <b>Note:</b> the <code class="inline language-javascript">show()</code> behavior implementation
    simply sets the <code class="inline language-javascript">display</code> property of the 
    <code class="inline language-javascript">dom_element</code>
    to <code class="inline language-javascript">block</code>. If block is not what you wish,
    you can either override the method in your own behavior class or add the
    <code class="inline language-html">data-component-display-value</code> attribute
    to your dom_element and set its value to whatever your prefer - for example 
    <code class="inline language-html">inline-block</code>. Then, the <code class="inline language-javascript">show()</code>
    behavior will use that instead of <code class="inline language-javascript">block</code>.

  %h2 Creating custom behaviors

  %p
    When designing your own component, you most certainly will want to create your own custom behaviors.
    To do that, you first need to define a class that extends
    <code class="inline language-javascript">ComponentBehaviors</code>:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class MyButtonComponentBehaviors extends extend_as("MyButtonComponentBehaviors").mix(ComponentBehaviors).with() {
          enlarge()       => this.dom_element.style.fontSize = "120%";
          normalizeSize() => this.dom_element.style.fontSize = "100%";
        }

  %p 
    Here we defined two behaviors that change our button font size (for whatever reason).
    Now we need add these behaviors to the list of behaviors in your component:

  %pre.line-numbers(data-line="2")
    %code.language-javascript
      :erb
        export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
          static get behaviors() { return [MyButtonComponentBehaviors]; }
          ...
        }

  %p
    The syntax for adding behavior to components may seem a bit odd - why use static getter?
    But it's due to limitations of Javascript, which doesn't currently have static
    class variables. And it makes sense to make the list of behaviors the same for all component
    instances.

  %p
    As you have probably guessed, you can define and add as many behavior classes as you want,
    combining different behaviors for various components, effectively making behavior classes modules:

  %pre
    %code.language-javascript
      :erb
          static get behaviors() { return [MyButtonComponentBehaviors, SomeOtherComponentBehavior]; }

  %p
    Now we can invoke our newly created behavior with:


  %pre
    %code.language-javascript
      :erb
        button1.behave("rockAndRoll");

  %p
    If you happen to have the same method in some of the behavior classes you've added to the behaviors list,
    then the most recent class in which this method is found will be used. In that sense, it works almost like inheritance.
    For example, suppose you have added the following behavior classes:


  %pre
    %code.language-javascript
      :erb
        static get behaviors() { return [BehaviorsOne, BehaviorsTwo, BehaviorsThree, BehaviorsFour]; }

  %p
    Let's say only <code class="inline language-javascript">BehaviorsOne</code> and
    <code class="inline language-javascript">BehaviorsThree</code>
    have a <code class="inline language-javascript">sayHello()</code> method you're about to call
    with <code class="inline language-javascript">behave("sayHello")</code>.
    In that case, <code class="inline language-javascript">BehaviorsThree</code>'s
    <code class="inline language-javascript">sayHello()</code> method will be invoked.


  %h2 Objects available to behavior instances

  %p
    When writing your own behavior class, you get access to the following
    objects:

  %ul
    %li 
      <code class="inline language-javascript">.component</code> - a reference to the component object
      this behavior is used for.
    %li
      <code class="inline language-javascript">.dom_element</code> - shorthand for
      <code class="inline language-javascript">.component.dom_element</code>
    %li
      <code class="inline language-javascript">.pos</code> - a refernce to the
      <code class="inline language-javascript">PositionManager</code> object that helps you easily
      manipulate the position of DOM elements.<br/>See #{link_to "Utils / Position Manager", "utils/position-manager"}
      page to learn more.
    %li
      <code class="inline language-javascript">.animator</code> - a reference to the
      <code class="inline language-javascript">Animator</code> object that helps you
      apply basic animations to DOM elements.<br/>See #{link_to "Utils / Animator", "utils/animator"}
      page to learn more.


  %h2 Do not modify components!
  %p
    To reduce coupling, behaviors should only read component attributes and use its own methods,
    but they should not be modifying component by changing its state (through attributes).
    If you find yourself writing a behavior class that does that, perhaps it's time to reassess
    whether what you're doing is a good idea.

  %p.note
    In the future, Webface.js will make component class only accessible through
    a read-only proxy, when accessed from by a behavior object, effectively preventing
    any changes to the component and throwing errors when changes are attempted.

  = partial "shared/nav", locals: { prev: "States/Transition ordering", nxt: "Templates/Overview" }
