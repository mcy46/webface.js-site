---
title: Standard components / Button - Webface.js
---

- @page_id = "standard-components/button"
.title
  %h1 Standard components / Button

.content

  %p.note
    #{link_to "Try a working example", "/examples/components/button"} of <code class="inline language-javascript">ButtonComponent</code>
    and then #{link_to "see the code for the example", "https://gitlab.com/hodlhodl-public/webface.js/tree/master/test/examples/components/button"}.

  %p
    A button is the most basic UI element you can probably think of, however
    in web apps it's largely implemented as some sort of secondary-class citizen - 
    reacting to events properly, but not adjusting to various use cases that largely
    depend on the context. For example, it is often common to use button as a simple
    link instead of a form submit element, but it's also obvious it should behave
    the same way (as a button!) when it's just a link - that is, appear to be pressed.
    Or, perhaps, it should behave like a real button in some ways, but not others.
    To understand which features of the <code class="inline language-javascript">ButtonComponent</code>
    are applicable in which cases it's better to do an overview of those features first, then learn how to
    use them, and then we can discuss situations in which those features can be applied.

  %h2 Button states

  %p
    While Webface.js lacks any kind of formal definition of "state",
    it is normally defined by component attributes, html attributes and, in particular,
    css classes assigned to a particular dom element.

  %p
    Button has three important states it can be in:

    %ul

      %li
        %h3 Normal
        %p
          Means a button is clickable and will publish the
          <code class="string">click</code> event if user clicks it.

      %li
        %h3 Locked
        %p
          Means a button is temporarily made
          inaccessible due to some action, typically user clicking that button.
          In this case, a <code class="inline language-html">locked</code> css class
          is added to the button's dom element.

        %h4 Usage
        %p
          You can toggle between having this
          state on and off with <code class="inline language-javascript">behave("lock")</code> and
          <code class="inline language-javascript">behave("unlock")</code>.
          By default, on <code class="inline language-javascript">click</code> event,
          <code class="inline language-javascript">ButtonComponent</code> invokes
          <code class="inline language-javascript">behave("lock")</code>. This default
          behavior can be disabled by setting writing <code class="inline language-javascript">button.set("lockable", false)</code>
          (can also be done through <code class="inline language-html">data-lockable="false"</code> attribute, it will
          update the corresponding <code class="inline language-javascript">ButtonComponent</code>
          attribute automatically on initialization). Normally, in your css,
          you'd want to add styles to the <code class="inline language-css">button.locked</code> selector,
          such that the button looks different from a normal one: for example has slightly less bright colors
          and a spinner is shown over the button, indicating that some processing is being performed.

        %p.note
          <code class="inline language-ruby">webface_rails</code> gem provides css styles for buttons
          along with the rest of the stylesheets for all of Webface.js standard components.

        %h4 Usecases
        %p
          Suppose your button doesn't invoke any actions that require some significant time to process - such as
          http requests or cryptographic algorithms - and it's potentially ok (and even expected)
          that users click it multiple times. For instance, if you have a form to purchase tickets for an event,
          it is possible that buyers would want to purchase multiple tickets for different people - we cannot predict how many,
          so we would add a button saying "Add attendee", which would add a row of form fields that are required
          for each attendee. In this case, it makes no sense to lock the button because adding new form fields
          is done very fast. The button, of course, would not be adding the fields itself, but merely
          publishing the <code class="inline language-javascript">click</code> event, while its parent - our custom form
          component of some sort, would capture the event and we'll write some code that would process this event.
          All the while, the only thing we'd need to do is add a simple html-attribute to the button's dom element:

        %script(type="text/plain" class="language-markup")
          :plain
            <button data-lockable="false">Add attendee</button>

        We won't need to change anything in <code class="inline language-javascript">ButtonComponent</code>
        for this to work.

      %li
        %h3 Disabled
        %p
          Means a button is not supposed to be clicked at all - typically because some condition, such as required form fields
          not being filled - is not satisfied.
        %ul
          %li
            %b Usage
            %p
              This state is defined by the <code class="inline string">disabled</code> attribute
              which can be set with both Javascript code and also in html by adding
              <code class="inline language-html">disabled="disabled"</code>
              to the button's dom element.
            %p.note
              Note how the previous "Locked" state didn't have a corresponding attribute in <code class="inline language-javascript">ButtonComponent</code>.
              <code class="inline language-javascript">.lockable</code> is a setting that affects locking the button,
              but not the state itself. And having the <code class="string">locked</code>
              attribute is not actually necessary - we just add or remove button dom element's css class in
              <code class="inline language-javascript">ButtonComponentBehaviors</code> to accomplish our goal. But
              with <code class="string">disabled</code> there <i>is</i>
              a component attribute we can (and should) change with <code class="inline language-javascript">ButtonComponent.set("disabled", [true/false])</code>

            %p
              To disable/enable the button, please use the component's attribute setter (instead of behaviors) like this:
            
            %pre
              %code.language-javascript
                :erb
                  button.set("disable", true);

            %p
              If you use behaviors instead, the button may appear disabled for users, but
              the value of the component's attribute will remain unchanged, which may affect
              your code.

  %h2 The click event and its handler
  
  %p
    The most common thing people do to a button is click it (or tap it, which highly depends
    on one's sexual preferences). Thus, the handler for this behavior has a number of things worth discussing:

  %ul
    %li
      <b>Event lock is set to the click event whenever a button is clicked</b>
      (unless the <code class="inline language-javascript">lockable</code> attribute was previously set to
      <code class="inline language-javascript">false</code>. If button is locked, either after it
      was clicked or because we locked it programmatically, it will not process any further
      <code class="inline language-javascript">click</code> events until it is unlocked. You MUST
      unlock it with <code class="inline language-javascript">ButtonComponent.behave("unlock")</code>
      manually from your code when you want to be back to processing new click events again -
      there's currently no default event handler which does it for you (because there's no reason for it).

    %li
      <b>You can prevent browser from invoking its default handler</b> for your button
      <code class="inline language-javascript">click</code> event by setting
      <code class="inline language-javascript">prevent_native_click_event</code> attribute to
      <code class="inline language-javascript">true</code>. This is useful in forms, for example,
      when you don't want to immediately submit a form, but rather validate it in the frontend and
      only submit that form after all validations pass. The default browser handler for the submit button,
      in this case, is submitting the form. We disabled this handler, while still instructing Webface.js to run
      our own handler code. This is so common (as buttons are mostly used in forms)
      that <code class="inline language-javascript">ButtonComponent</code> defaults to preventing native
      browser event handler for the <code class="inline language-javascript">click</code> event for all of
      its instances. However, it is also desirable to enable default browser handler when a button
      acts as a link. It can be done by adding an appropriate html-attribute:

      %script(type="text/plain" class="language-markup")
        :plain
          <a href="/offers" data-prevent-native-click-event="false">All offers</a>

      %p.note
        If you're using webface_rails, there's a special helper to generate buttons
        that act as links: <code class="inline language-ruby">button_link</code>. See
        #{link_to "Ruby On Rails integration / Standard components views", "/ruby-on-rails-integration/standard-components-views.html"}
        for more info.

      %p.note
        If you're not using webface_rails, don't forget to set
        <code class="inline language-html">data-prevent-native-click-event="false"</code> on the
        submit buttons (if that's indeed what you want). TODO: perhaps the default
        attribute setting should be changed to <code class="inline language-html">data-prevent-naive-click-event="false"</code>
        to avoid confusion, while setting the attribute value to <code class="inline language-javascript">true</code>
        for all buttons that are not links in webface_rails.

  = partial "shared/nav", locals: { prev: "Templates/Named templates", nxt: "Standard components/ConfirmableButton" }
