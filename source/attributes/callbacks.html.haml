---
title: Attributes / Callbacks - Webface.js
---

- @page_id = "attributes/callbacks"
.title
  %h1 Attributes / Callbacks

.content

  %p
    Sometimes writing attribute value to DOM upon attribute change might not be enough. Let's say you want to display
    a warning to the user whenever button becomes disabled, so the user isn't too frustrated as to why that happened
    (perhaps, it happened because he clicked that button to send the form and we don't want him to click it again and
    re-send the form accidentally).

  %p We'd then write the following code for our component:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
          constructor() {
            this.attribute_names = ["disabled"];
            this.attribute_callbacks["disabled"] = (attr_name, self) => {
              alert(`${self.constructor.name} ${attr} has changed, new value is ${self.get(attr_name)}`);
            }
        }

  %p
    Now, if anywhere in the code we write <code class="inline language-javascript">button1.set("disabled", "disabled")</code>
    (assuming <code class="inline language-javascript">button1</code> contains our component instance),
    it will display the alert saying <i>"MyButtonComponent disabled has changed, new value is disabled"</i>.
    The problem with this code, however, is that because we defined a custom callback for attribute
    <code class="inline language-javascript">disabled</code>, the default one stopped working. And here's the default callback
    for all attributes, as defined by the code in Webface.js's <code class="inline language-javascript">Component</code> class:

  %pre.line-numbers
    %code.language-javascript
      :erb
        this.attribute_callbacks = {
          'default' : (attr_name, self) => {
            this.constructor.attribute_callbacks_collection['write_attr_to_dom'](attr_name, self);
          }
        };

  %p
    As you can see, the default behavior here is to write value to DOM. But it will not run since indeed we defined a custom
    callback for the <code class="inline language-javascript">disabled</code> attribute. To have it both ways - display our alert
    and update DOM - we can add this <code class="inline language-javascript">write_attr_to_dom()</code> call very easily to our
    callback:

  %pre.line-numbers(data-line="5")
    %code.language-javascript
      :erb
        export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
          constructor() {
            this.attribute_names = ["disabled"];
            this.attribute_callbacks["disabled"] = (attr_name, self) => {
              self.constructor.attribute_callbacks_collection['write_attr_to_dom'](attr_name, self);
              alert(`${self.constructor.name} ${attr} has changed, new value is ${self.get(attr_name)}`);
            }
        }

  %p
    Note how we used <code class="inline language-javascript">self</code> instead of <code class="inline language-javascript">this</code>
    because <code class="inline language-javascript">this</code> would not refer to current component in this context.

  %p Theoretically, we could've written something like

  %pre.line-numbers(data-line="5")
    %code.language-javascript
      :erb
        export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
          constructor() {
            this.attribute_names = ["disabled"];
            this.attribute_callbacks["disabled"] = (attr_name, self) => {
              self.dom_element.setAttribute("disabled", "disabled");
              alert(`${self.constructor.name} ${attr} has changed, new value is ${self.get(attr_name)}`);
            }
        }

  %p which looks nicer, but the problem is that it becomes very component specific, while this line

  %pre.line-numbers
    %code.language-javascript
      :erb
        self.constructor.attribute_callbacks_collection['write_attr_to_dom'](attr_name, self);

  %p offers a standard way of updating DOM that goes through all the necessary hoops.

  %h2 Defining your custom callback functions

  %p.note
    %b DISCLAIMER:
    attribute callback functions look very complex right now (too much code!), and it is currently in the TODO
    to simplify it a little bit, so it starts making more sense. Consider current state of things a preliminary low-level version
    of Webface.js API.

  %p
    As you could've probably guessed, you can define your own callback functions, which can be applied to many different
    attributes. For example, we could've had this function defined:

    %pre.line-numbers
      %code.language-javascript
        :erb
          export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
            constructor() {
              this.constructor.attribute_callbacks_collection['display_warning'] = (attr_name, self) => {
                alert(`${self.constructor.name} ${attr} has changed, new value is ${self.get(attr_name)}`);
              };
              ...
            }
          }

    %p Now we can call this function inside callbacks for various fields:

    %pre.line-numbers
      %code.language-javascript
        :erb
          export class MyButtonComponent extends extend_as("MyButtonComponent").mix(Component).with() {
            constructor() {
              this.attribute_names = ["disabled", "caption"];
              this.attribute_names.forEach((attr_name) => {
                this.attribute_callbacks[attr_name] = (attr_name, self) => {
                  self.constructor.attribute_callbacks_collection['write_attr_to_dom'](attr_name, self);
                  self.constructor.attribute_callbacks_collection['display_warning'](attr_name, self);
                }
              });
            }
          }

    %p
      Now all attributes will call <code class="inline language-javascript">write_attr_to_dom</code> AND
      <code class="inline language-javascript">display_warning</code> callbacks when changes to component attributes
      are detected.

    %p
      There isn't any specific reason why you can't have your callback function stored in elsewhere and not
      in <code class="inline language-javascript">self.constructor.attribute_callbacks_collection</code>,
      however it's just a convenient standard place to put those functions and therefore it is recommended
      you put it there.

    %p
      To be fair, it is not very often that you'd actually need to define your custom attribute callback functions.
      More often than not you would just find yourself writing custom callbacks and including the
      <code class="inline language-javascript">self.constructor.attribute_callbacks_collection['write_attr_to_dom'](attr_name, self);</code>
      in those callbacks when needed.

  = partial "shared/nav", locals: { prev: ["Attributes/", "Default values", "./default-values"], nxt: ["Attributes/", "Casting", "./casting"]}
