---
title: Ruby On Rails integration / Reusing component views - Webface.js
---

- @page_id = "ruby-on-rails-integration/reusing-component-views"
.title
  %h1 Ruby On Rails integration / Reusing component views

.content

  %p
    Very often you will find that you need to reuse the components you've already defined in some of your views.
    For instance, looking at the example from the #{link_to "previous page", "./defining-component-views.html"}, we could find ourselves
    needing to reuse both <code class="inline language-ruby">:offer_list</code> and
    <code class="inline language-ruby">:offer</code> components in two places in our Rails app:
    on the "Offers" page that's accessible to all users and also on
    the "My Offers" page that can only be seen by the currently signed in user.

  %p
    You may rightfully say, that for these purposes, a partial could be used, however with a partial you'd need to pass
    all these local variables to set component attributes, html attributes and other things. Thus,
    <code class="inline language-ruby">webface_rails</code> helpers simplify this process.

  %p
    What we actually want to write on both "Offers" and "My Offers" pages is something like this:

  %pre.line-numbers
    %code.language-haml
      :erb
        = component :offer_list, per_page: 20 do
          - @offers.each do |offer|
            = component :offer, disabled: offer.is_disabled, title: @offer.title, description: @offer.description

  %p
    To be able to do that, we need to create two files in the
    <code class="inline language-ruby">app/views/webface_components</code> directory. Let's start with the offer:

  %pre.line-numbers
    %code.language-haml
      :erb
        /- app/views/webface_components/offer.html.haml
        = define_component :offer, ".offer", {      |
            attr_map: "disabled:disabled",          |
            disabled: false,                        |
            style: "font-weight: bold",             |
            mapped_attrs: { visible: true }         |
          }, options.except(:title,:description) do |

        = component_part :body do
          = component_attr :title, "span", text_content: options[:title]
          = component_attr :description, "span", text_content: options[:description]

  %p
    Notice a few changes from the previous page example. First of all, we've added a fourth argument to the
    <code class="inline language-ruby">define_component</code> method call, also a hash called options. And, the previous
    so called options are now wrapped in <code class="inline language-ruby">{}</code> effectively making it
    the third argument. But why?

  %p
    This so we can specify default options in the this argument, but then anything that's passed in the
    <code class="inline language-ruby">= component</code> call will be merged into the third argument,
    replacing default arguments. The <code class="inline language-ruby">options</code> local variable itself
    is available in this view context because that's what <code class="inline language-ruby">webface_rails</code>
    does for us when we reuse components with <code class="inline language-ruby">= component</code> method - it actually
    renders a partial and passes all of the named arguments as hash into this <code class="inline language-ruby">options</code>
    variable.

  %p Confused? Ok, let's try again. Here's our <code class="inline language-ruby">= component</code> method call:

  %pre
    %code.language-haml
      :erb
        = component :offer, disabled: offer.is_disabled, title: @offer.title, description: @offer.description

  %p
    Suppose <code class="inline language-ruby">offer.disabled</code> is <code class="inline language-ruby">true</code>
    in this case. Then, within that <code class="inline language-shell">app/views/webface_components/offer.html.haml</code>
    file, the <code class="inline language-ruby">options</code> variable will contain the following:

  %pre.line-numbers
    %code.language-ruby
      :erb
        {
          disabled: true,
          title: "... value from @offer.title",
          description: "... value from @offer.description"
        }
  %p
    When <code class="inline language-ruby">= define_component</code> is called and is passed
    <code class="inline language-ruby">options</code> it automatically merges it with the hash in the third argument.
    The interesting part, though, is how we exclude <code class="inline language-ruby">:title</code>
    and <code class="inline language-ruby">:description</code> from the <code class="inline language-ruby">options</code>
    hash before merging it. Why? Because we want the Webface component binding of those attributes to
    be on some of the child dom elements. If we included them, we'd end up with the the following html:

  %script(type="text/plain" class="language-markup")
    :plain
      <div class="offer" data-component-class="OfferComponent" title="..." description="...">
        ...
      </div>

  %p
    That is hardly our intention. <code class="inline language-ruby">:title</code>
    and <code class="inline language-ruby">:description</code> are getting their own dom elements, specifically these:

  %pre.line-numbers
    %code.language-haml
      :erb
        = component_attr :title, "span", text_content: options[:title]
        = component_attr :description, "span", text_content: options[:description]

  %p
    If description required some more customization, we could pass
    the <code class="inline language-ruby">options[:description]</code> in a block like this:

  %pre.line-numbers
    %code.language-haml
      :erb
        = component_attr :title, "span", text_content: options[:title]
        = component_attr :description, "span" do
          %h3 Description:
          %p= options[:description]

  %p
    Now finally, let's take care of our <code class="inline language-ruby">:offer_list</code> component view
    in the same manner, by creating a file in <code class="inline language-shell">app/views/webface_components</code>

  %pre.line-numbers
    %code.language-haml
      :erb
        /- app/views/webface_components/offer_list.html.haml
        = define_component :offer_list, ".offerList", {     |
            mapped_attrs: { per_page": 10, visible: true }, |
          }, options do                                     |
          = capture(&block)

  %p
    Notice how on line 5 here, we called <code class="inline language-ruby">capture(&block)</code>.
    This is because we know that when this component view is reused in some other view
    with <code class="inline language-ruby">component :offer_list</code>
    it will be passed a block that includes the content (individual offers). If we forgot to add
    the <code class="inline language-ruby">capture(&block)</code> call, we'd have an empty offer list.


  = partial "shared/nav", locals: { prev: "Ruby On Rails integration/Defining component views", nxt: "Ruby On Rails integration/Helper options" }
