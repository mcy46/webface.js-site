import extend_as                  from '../webface/lib/utils/mixin.js'
import Component                  from '../webface/lib/component.js'
import RootComponent              from '../webface/lib/components/root_component.js'
import MenuItemComponentBehaviors from '../behaviors/menu_item_component_behaviors.js'

export default class MenuItemComponent extends extend_as("MenuItemComponent").mix(Component).with() {

  static get behaviors() { return [MenuItemComponentBehaviors]; }

  constructor() {
    super();

    this.attribute_names = ["type", "page_id", "title"];
    this.native_events   = ["click", "link.click"];
    this.no_propagation_native_events = ["click"];

    this.event_handlers.addForEvent("click", {
      "#self": (self,event) => {
        if(event.target == self.dom_element && self.get("type") == "parent") self.behave("toggle");
      },
      "self.link": (self,event) => {
        if(self.get("type") == "parent")
          self.behave("toggle")
        else
          window.location.href = event.target.getAttribute("href");
      }
    });

  }

  afterInitialize() {
    super.afterInitialize();
    this.updateAttrsFromNodes();

    if(this.get("page_id") == document.querySelector("body").getAttribute("data-page-id")) {

      this.afterParentInitialized("#self", (self, publisher) => {

        let parents = [];
        let prnt    = this.parent;
        while(prnt.constructor.name == "MenuItemComponent") {
          parents.push(prnt);
          prnt = prnt.parent;
        }

        if(parents.length > 0)
          parents.forEach((prnt) => prnt.behave("expand"));

      });

      this.behave("makeCurrent");
    }
  }


}

window.webface.component_classes["MenuItemComponent"] = MenuItemComponent;
