import extend_as         from '../webface/lib/utils/mixin.js'
import Component         from '../webface/lib/component.js'
import RootComponent     from '../webface/lib/components/root_component.js'
import MenuItemComponent from './menu_item_component.js'

export class SidebarComponent extends extend_as("SidebarComponent").mix(Component).with() {

  constructor() {
    super();

    this.native_events = ["border.mousedown", "border.mouseup"];

    this.event_handlers.addForRole("self.border", {
      mousedown: this._startResize
    });

    this.event_handlers.addForRole("search", {
      change: (self,child) => self.filterMenu(child.get("value"))
    });

  }

  afterInitialize() {
    super.afterInitialize();
  }

  filterMenu(s) {
    var items = this.findAllDescendantInstancesOf(MenuItemComponent, { continue_search_deeper_when_found: true });

    if(s == null) {
      items.forEach((i) => {
        i.behave("show");
        i.behave("dehighlight");
      });
      return;
    }

    var items_to_hide = [];

    items.forEach((i) => {
      i.behave("dehighlight");
      if(!(i.get("title").toLowerCase().includes(s.toLowerCase())))
        items_to_hide.push(i);
    });

    var items_to_show = [];
    items.forEach((i) => {
      if(i.get("title").toLowerCase().includes(s.toLowerCase())) {
        i.behave("highlight");
        items_to_show.push(i);
        items_to_hide = items_to_hide.filter((item_to_hide) => item_to_hide.get("title") != i.get("title"));
        let prnt = i.parent;
        while(prnt.constructor.name == "MenuItemComponent") {
          items_to_show.push(prnt);
          prnt.behave("expand");
          items_to_hide = items_to_hide.filter((item_to_hide) => item_to_hide.get("title") != prnt.get("title"));
          prnt = prnt.parent;
        }

        let children = i.findAllDescendantInstancesOf(MenuItemComponent, { continue_search_deeper_when_found: true });
        children.forEach((c) => {
          items_to_hide = items_to_hide.filter((item_to_hide) => item_to_hide.get("title") != c.get("title"));
          items_to_show.push(c);
        });
      }
    });

    items_to_hide.forEach((i) => i.behave("hide"));
    items_to_show.forEach((i) => i.behave("show"));

  }

  _startResize(self, e) {
    self.prev_resize_mouse_position = e.screenX;
    let root = RootComponent.instance;
    root.native_events.push("mouseup");
    root.native_events.push("mousemove");
    root.dom_element.style.cursor = "ew-resize";
    root.event_handlers.addForRole("#self", { mouseup: (root,e) => self._stopResize(root), mousemove: (root,e) => self._moveBorder(e) });
    root.reCreateNativeEventListeners();
  }

  _stopResize(root) {
    root.event_handlers.removeForRole("#self", ["mouseup", "mousemove"]);
    root.dom_element.style.cursor = "auto";
  }

  _moveBorder(e) {
    let x         = e.screenX;
    let new_width = this.dom_element.getBoundingClientRect().width + x - this.prev_resize_mouse_position;
    console.log(this.dom_element.getBoundingClientRect().width, x);
    this.dom_element.style.width = new_width + "px";
    this.prev_resize_mouse_position = x;
  }

}

window.webface.component_classes["SidebarComponent"] = SidebarComponent;
