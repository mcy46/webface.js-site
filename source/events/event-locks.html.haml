---
title: Events / Event locks - Webface.js
---

- @page_id = "events/event-locks"
.title
  %h1 Events / Native vs Custom events

.content

  %p
    Sometimes you want an event to only be handled once and ignore all the subsequent events. For instance, when a user
    clicks the submit button on a form, you want it to handle the first click, but ignore the subsequent ones, because that
    could trigger the form to be submitted twice. Event locks are designed specifically for this purpose.
    Let's look at our previous example with <code class="inline language-javascript">FormButtonComponent</code>:

  %pre.line-numbers(data-line="8,9")
    %code.language-javascript
      :erb
         export class FormButtonComponent extends extend_as("FormButtonComponent").mix(Component).with() {
          constructor() {
            super();

            this.native_events = ["!submit.click", "!cancel.click"];
            this.event_lock    = ["submit.click"];

            this.event_handlers.addForEvent("click", {
              "self.submit" : (self,event) => {
                console.log("Button clicked");
                self.publishEvent('submit');
              },
              "self.cancel" : (self,event) => self.publishEvent('cancel')
            }); 
          }
        }

  %p
    Now if the user clicks this button twice, we'll only handle the first click and only one
    <code class="inline language-javascript">"Button clicked"</code> message will
    be printed to the console. This however implies that all buttons behave the same way -
    that is, ignore all subsequent clicks. Perhaps that's not what you want.
    Perhaps what you really want is for forms to not be submitted twice. In this case, we could write the
    following code in the <code class="inline language-javascript">UserFormComponent</code>:

  %pre.line-numbers(data-line="5,6")
    %code.language-javascript
      :erb
         export class UserFormComponent extends extend_as("UserFormComponent").mix(Component).with() {
          constructor() {

            super();
            // This is moved here from FormButtonComponent
            this.event_lock = ["submitter.click"];

            this.event_handlers.addForRole("submitter", {
              "submit" : (self,event) => {
                console.log("Button clicked");
                self.publishEvent('submit');
              },
              cancel: (self,event) => console.log("Form submission cancelled")
            }); 
          }
        }


  %p
    And now you will be able to click buttons as many times as you want in other places, but if this button belongs to a
    form, the form will ignore all subsequent clicks (of course, we no longer need the event_lock_for statement in
    <code class="inline language-javascript">ButtonComponent</code>). To once again enable the form to start handling
    click events you can remove the event from the event_locks list:

  %pre
    %code.language-javascript
      :erb
        user_form.event_locks.remove('submitter.click');

  %p
    You can also set event locks manually using either <code class="inline language-javascript">addEventLock(</code>)
    method or <code class="inline language-javascript">event_locks</code> property directly.
    The difference is that the first one will additionally check whether the property for which the lock is being
    added is in the <code class="inline language-javascript">event_lock_for</code> list - and if not, the lock won't be added.
    If you want to add the lock anyway, you can just use <code class="inline language-javascript">event_locks</code> property
    and its <code class="inline language-javascript">.add</code> method (because it's a Javascript Set object!). Let's see an example:

  %pre.line-numbers
    %code.language-javascript
      :erb
        export class FormButtonComponent extends extend_as("FormButtonComponent").mix(Component).with() {
          constructor() {
            super();
            this.native_events = ["!submit.click", "!cancel.click"];
            this.event_lock    = ["submit.click"];
            ...
          }
        }

        var button = new FormButtonComponent();
        button.addEventLock("click");        // <-- event lock for click added, button will not be clicked twice
        button.addEventLock("mouseover");    // <-- lock NOT added, because "mouseover" is not in `event_lock_for`
        button.event_locks.add("mouseover"); // <-- event lock for mouseover event added
        button.event_locks.remove("button"); // <-- button can be clicked again!

  = partial "shared/nav", locals: { prev: "Events/Native vs Custom events", nxt: "States/Overview" }
